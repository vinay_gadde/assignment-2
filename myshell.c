
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char **argv)
{
    // Shell prompt message when waiting for input or displaying
    const char SHELL_MESSAGE[] = "myshell";

    // Word and character limits for user inputs
    const int SHELL_INPUT_WORD_LIMIT = 20;
    const int SHELL_INPUT_CHAR_LIMIT = 10 * SHELL_INPUT_WORD_LIMIT;

    // Delimiters for tokenizing user input - space, new line, tabs
    const char TOKEN_DELIM[] = " \n\t";

    // FLag to keep shell activated
    bool shell_activated = true;

    // Loop to keep shell active
    while (shell_activated)
    {

        char line[SHELL_INPUT_CHAR_LIMIT];     // String to get user input
        char *command[SHELL_INPUT_WORD_LIMIT]; // Array of strings to store tokenized input
        char *token_sc;                        // String to store a single token of tokenized string by semi colon
        char *subcommand;                      // String to store a subcommand separated by semicolon,reference of token_sc
        char *token;                           // String to store a single token of tokenized string by spaces
        int position;                          // Position of the current token
        char *operation;                       // String to store operation i.e. first token
        char **params;                         // String to store parameters i.e. string after first token

        printf("%s> ", SHELL_MESSAGE);              // Prints shell prompt myshell>
        fgets(line, SHELL_INPUT_CHAR_LIMIT, stdin); // Get user input in line

        subcommand = token_sc = strtok(line, ";\n");

        // If there is no input, reset shell
        if (token_sc == NULL)
        {
            continue;
        }
        
        // Tokenize user input by delimiter spaces, returns first token
        // Reentrant version of strtok is used since we need to keep track of tokenization of two strings at the same time
        // Reference subcommand is used and token_sc is preserved for further usage        
        token = strtok_r(subcommand, TOKEN_DELIM, &subcommand);
        

        // First token is the operation
        operation = token;

        // If operation is exec, execute shell commands
        if (strcmp(operation, "exec") == 0)
        {
            // Loop until there are no more semi-colon delimited commands and point to next token
            for(;token_sc != NULL; subcommand = token_sc = strtok(NULL, ";\n"))
            {
                int status;
                position = 0;
                
                // Tokenize subcommand by spaces
                token = strtok_r(subcommand, TOKEN_DELIM, &subcommand);
                
                // put tokens in array command of strings
                command[position] = token;
                while (token != NULL)
                {
                    token = strtok_r(subcommand, TOKEN_DELIM, &subcommand);
                    command[++position] = token;
                }
                
                // If first parameter (after operation) is empty, Continue to next iteration
                if(command[0] == NULL)
                {
                    continue;
                }

                // Start a child process to execute shell commands, take process id
                pid_t pid = fork();

                // If process id is returned -1 (or negative), there is a error creating child
                if (pid < 0)
                {
                    printf("%s: ", SHELL_MESSAGE);
                    perror("Child Process Fork Failed");
                    exit(errno);
                }
                // If this is a parent process, we have child process id in pid
                else if (pid > 0)
                {
                    // Print child process id and then wait for child process to complete
                    printf("%s: starting child pid %d\n", SHELL_MESSAGE, pid);
                    wait(&status);
                }

                // If this is the child process, it has process id as 0
                else if (pid == 0)
                {
                    execvp(command[0], command); // Execute parameters as shell command
                    perror(command[0]);         // Print error if any for the shell command
                    exit(errno);               // Exit the child process with error if any
                }
            }
        }

        // If operation is exit, exit shell by setting flag to false
        else if (strcmp(operation, "exit") == 0)
        {
            shell_activated = false;
        }

        // If the operation is not among exec or exit, print error and then reset shell
        else
        {
            printf("%s: %s is not a valid operation\n", SHELL_MESSAGE, operation);
        }
    }

    // Return successfully
    return 0;
}
